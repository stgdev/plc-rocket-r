// POSAssist.cpp : Defines the entry point for the application.
//

#include "stdafx.h"

#if defined(__UNIX__)
#  include <unistd.h>
#  include <fcntl.h>
#  include <sys/time.h>
#elif defined(__WIN32__)
#  include <windows.h>
#  include <io.h>

#include <CommDlg.h>
#include <atlstr.h>
#endif

#include "DevConnect\sio.h"
#include "DevConnect\sock.h"
#include "DevConnect\pipe.h"
#include "DevConnect\thread.h"
#include "DevConnect\vlist.h"
#include "DevConnect\cfglib.h"
#include "DevConnect\config.h"
#include "DevConnect\error.h"

#include "Devices\RFID_SL500\MifareOne.h"

#include "netcommand.h"
#include "POSAssist.h"
#include <string>
using namespace std;

//data MDB
#include <oledb.h>  
#include <stdio.h>  
#include <conio.h>  
#include "icrsint.h" 
#import <C:\\Program Files\\Common Files\\System\\ado\\msado15.dll> rename( "EOF", "AdoNSEOF" )
//Microsoft.ACE.OLEDB.12.0
#include <ole2.h>  
_bstr_t bstrConnect = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=..\\database\\database.mdb;";
HRESULT hr = S_OK;;
//chana do
string datamockup[3][2] = {
	{"0","AAA"},
	{"1","BBB"},
	{"2","CCC"}
};
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG		msg;
	HACCEL	hAccelTable;
	HRESULT	hResult;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_POSASSIST, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_POSASSIST));

	// Initialize system tray icon
	iconSystemTray.cbSize = sizeof(iconSystemTray);
	iconSystemTray.hWnd   = hWndPOSAssist;
	iconSystemTray.uID	  = IDI_POSASSIST;
	iconSystemTray.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	iconSystemTray.hIcon  = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_POSASSIST));
	iconSystemTray.uCallbackMessage = WM_POSASSIST_SYSTRAY;
	hResult = StringCchPrintf(iconSystemTray.szTip,
							  MAX_LOADSTRING,
							  TEXT("SMIT POSAssist"));
	if (FAILED(hResult))
		OutputDebugString(TEXT("Can not create system icon TIP"));

	Shell_NotifyIcon(NIM_ADD, &iconSystemTray);
	bHideWnd = FALSE;
	HideWindow(); /** By default, this window must be hidden */

	//database MDB
	ADODB::_ConnectionPtr pConn("ADODB.Connection");
	hr = pConn->Open(bstrConnect, "", "", ADODB::adConnectUnspecified);
	if (SUCCEEDED(hr)) {
		//test write 0
		//test read 1
		int mode = 1;
		_bstr_t query;
		if (mode == 0) {
			query = "SELECT * FROM MOCKUP;";
			ADODB::_RecordsetPtr pRS("ADODB.Recordset");
			hr = pRS->Open(query,
				_variant_t((IDispatch*)pConn, true),
				ADODB::adOpenForwardOnly,
				ADODB::adLockReadOnly,
				ADODB::adCmdText);
			if (SUCCEEDED(hr)) {
				ADODB::Fields* pFields = NULL;
				hr = pRS->get_Fields(&pFields);

				if (SUCCEEDED(hr) && pFields && pFields->GetCount() > 0) {
					// ID DATA header
					for (long nIndex = 0; nIndex < pFields->GetCount(); nIndex++) {
						//dbg_msg(TEXT("Successfully connected to database. %s\n"), _bstr_t(pFields->GetItem(nIndex)->GetName()));
						dbg_msg(_bstr_t(pFields->GetItem(nIndex)->GetName()));
					}
				}
				else {
					dbg_msg(TEXT("Error: Number of fields in the result set is 0.\n"));
				}
				int rowCount = 0;
				//data in header
				while (!pRS->AdoNSEOF) {
					for (long nIndex = 0; nIndex < pFields->GetCount(); nIndex++) {
						dbg_msg(_bstr_t(pFields->GetItem(nIndex)->GetValue()));
					}
					pRS->MoveNext();
					rowCount++;
				}
				///cout << DAM << ": Total Row Count: " << rowCount << endl;
			}
		}
		else {
			query = "INSERT INTO MOCKUP(DATA) VALUES ('WWW');";
			ADODB::_RecordsetPtr pRS("ADODB.Recordset");
			hr = pRS->Open(query,
				_variant_t((IDispatch*)pConn, true),
				ADODB::adOpenForwardOnly,
				ADODB::adLockReadOnly,
				ADODB::adCmdText);
			if (SUCCEEDED(hr)) {
				ADODB::Fields* pFields = NULL;
				dbg_msg(TEXT("sucess"));
			}
			//pRS->Close();
		}
		pConn->Close();
		//cout << DAM << ": Cleanup. Done." << endl;
	}
	else {
		OutputDebugString(TEXT("Database connect fail.\n"));
	}

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_POSASSIST));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_POSASSIST);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;
   RAWINPUTDEVICE *Rid= new RAWINPUTDEVICE[1];

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   // Register USB Notification
   Rid[0].usUsagePage = 0x01; // Generic Desktop Page 
   Rid[0].usUsage     = 0x06; // Keyboard

   // Refer to USB.org documents
   //Rid[0].usUsagePage = 0x8C; // Barcode Scanner
   //Rid[0].usUsage     = 0x02; // (Both setting UNABLE TO USE NOW)

   Rid[0].dwFlags     = RIDEV_INPUTSINK;
   Rid[0].hwndTarget  = hWnd; 
   if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE) {
	   OutputDebugString (TEXT("Can NOT Register RID.\n"));
   }
   else {
	   OutputDebugString (TEXT("Can Register RID.\n"));
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   hWndPOSAssist = hWnd; // It's important to set hWndPOSAssist = hWnd
						 // before calling below functions.
   SearchDevice();
   dbg_msg(TEXT("POSAssist system boot up ...\n"));
   if (preWaitClients() != 0) 
	   return FALSE;

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static SOCKET callSocket;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;

	case WM_POSASSIST_SYSTRAY:
		SystemTrayAct(lParam);
		break;
	case WM_INPUT:
		{
			UINT dwSize;
			
			GetRawInputData((HRAWINPUT)lParam, 
							RID_INPUT, 
							NULL, 
							&dwSize, 
							sizeof(RAWINPUTHEADER));

			LPBYTE lpb = new BYTE[dwSize];
			if (lpb == NULL) 
			{
				return 0;
			} 

			if (GetRawInputData((HRAWINPUT)lParam, 
								RID_INPUT, 
								lpb, 
								&dwSize,
								sizeof(RAWINPUTHEADER)) != dwSize )
				OutputDebugString (TEXT("GetRawInputData does not return correct size !\n")); 

			RAWINPUT* raw = (RAWINPUT*)lpb;
			if ((raw->header.hDevice == hBarcodeScanner) && // Store data for difined BCS only
				(raw->data.keyboard.Flags == 0x01)) {       // KeyDown: 0x00 KeyUp: 0x01
				HRESULT   hResult;
				
				hResult = StringCchPrintf(szStoreCh, 
								MAX_LOADSTRING, 
								TEXT("%c"), 
								raw->data.keyboard.VKey);
				/*OutputDebugString (TEXT("BCS I/P: ")); // Uncomment for DEBUGGING */
				if (FAILED(hResult))
					OutputDebugString (TEXT("GetRawInputData does not return correct size! \n"));
				/*else
					OutputDebugString (szStoreCh);
				OutputDebugString (TEXT(" \n")); // Uncomment for DEBUGGING */

				/** Manage BCS Line: */
				if (VK_RETURN != raw->data.keyboard.VKey) {
					StringCchCat(szScanData, MAX_LOADSTRING, szStoreCh);
				}
				else {
					/** Currently, we wont add the RETURN key to szScanData */
					hResult = StringCchPrintf(szBSCFIFO,
								MAX_LOADSTRING,
								TEXT("%s"),
								szScanData);
					if (FAILED(hResult))
						OutputDebugString (TEXT("Can not make BCS Line text."));
					else
						dbg_msg(TEXT("szBSCFIFO:%s <--- szScanData:%s \n"),
								szBSCFIFO, szScanData);	/** Ready for POS Request! */

					/** Clean up szScanData */
					hResult = StringCchPrintf(szScanData,
											MAX_LOADSTRING,
											TEXT(""));
					if (FAILED(hResult))
						OutputDebugString (TEXT("Can not clean up szScanData.\n"));
				
				}

			}
		}
		break;

	case WM_POSASSIST_SOCKET:
		{
			switch (WSAGETSELECTEVENT(lParam)) {
				case FD_CLOSE:
					OutputDebugString(TEXT("FD_CLOSE Lost the connection\n"));
					break;
				case FD_ACCEPT:
					OutputDebugString(TEXT("FD_ACCEPT Connection request was made\n"));
					callSocket = (SOCKET)findInvolvedSocket((SOCKET)wParam);
					break;
				case FD_READ:
					OutputDebugString(TEXT("FD_READ Incomming data; get ready to receive\n"));
					/**
					 To protect RACE-CONDITION between Async Socket(WM_POSASSIST_SOCKET) & 
					 FD_SET/FD_ISET(serve_pipe_ext), we send WM_CHK_SOCKINPUT in serve_pipe_ext()
					 to check data/command from socket input.
					*/
					break;
				case FD_WRITE:
					OutputDebugString(TEXT("FD_WRITE\n"));
					break;
			}
		}
		break;

	case WM_CHK_SOCKINPUT:
		dbg_msg(TEXT("WM_CHK_SOCKINPUT was made!\n"));
		isCMDSockRAW = FilterSocketRAW((SOCKET)wParam);
		break;

	case WM_DESTROY:
		/** Smilar to WM_CLOSE */
		dbg_msg(TEXT("POSAssist System Close was requested.\n"));
		Shell_NotifyIcon(NIM_DELETE, &iconSystemTray);

		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// Search supported device
INT SearchDevice()
{
	UINT nDevices = 0;
	GetRawInputDeviceList( NULL, &nDevices, sizeof( RAWINPUTDEVICELIST ) );

	// Got Any?
	if( nDevices < 1 )
	{
		// Exit
		OutputDebugString (TEXT("ERR: 0 Devices?\n"));
		return 0;
	}

	// Allocate Memory For Device List
	PRAWINPUTDEVICELIST pRawInputDeviceList;
	pRawInputDeviceList = new RAWINPUTDEVICELIST[ sizeof( RAWINPUTDEVICELIST ) * nDevices ];

	// Got Memory?
	if( pRawInputDeviceList == NULL )
	{
		// Error
		OutputDebugString (TEXT("ERR: Could not allocate memory for Device List.\n"));
		return -1;
	}
	
	// Fill Device List Buffer
	int nResult;
	nResult = GetRawInputDeviceList( pRawInputDeviceList, &nDevices, sizeof( RAWINPUTDEVICELIST ) );

	// Got Device List?
	if( nResult < 0 )
	{
		// Clean Up
		delete [] pRawInputDeviceList;

		// Error
		OutputDebugString (TEXT("ERR: Could not get device list."));
		return -2;
	}

	// Loop Through Device List
	for( UINT i = 0; i < nDevices; i++ )
	{
		// Get Character Count For Device Name
		UINT nBufferSize = 0;
		nResult = GetRawInputDeviceInfo( pRawInputDeviceList[i].hDevice, // Device
										 RIDI_DEVICENAME,				 // Get Device Name
										 NULL,							 // NO Buff, Want Count!
										 &nBufferSize );				 // Char Count Here!

		// Got Device Name?
		if( nResult < 0 )
		{
			// Error
			OutputDebugString (TEXT("ERR: Unable to get Device Name character count.. Moving to next device.\n\n"));

			// Next
			continue;
		}

		// Allocate Memory For Device Name
		WCHAR* wcDeviceName = new WCHAR[ nBufferSize + 1 ];
		
		// Got Memory
		if( wcDeviceName == NULL )
		{
			// Error
			OutputDebugString (TEXT("ERR: Unable to allocate memory for Device Name.. Moving to next device.\n\n"));

			// Next
			continue;
		}

		// Get Name
		nResult = GetRawInputDeviceInfo( pRawInputDeviceList[i].hDevice, // Device
										 RIDI_DEVICENAME,				 // Get Device Name
										 wcDeviceName,					 // Get Name!
										 &nBufferSize );				 // Char Count

		// Got Device Name?
		if( nResult < 0 )
		{
			// Error
			OutputDebugString (TEXT("ERR: Unable to get Device Name.. Moving to next device.\n\n"));

			// Clean Up
			delete [] wcDeviceName;

			// Next
			continue;
		}
		
		WCHAR *wcTarget = new WCHAR[30 + 1];
		TCHAR OpticonScanner[30] = TEXT("\\\\?\\HID#VID_065A&PID_0001");
		TCHAR SymbolScanner[30]  = TEXT("\\\\?\\HID#VID_05E0&PID_1200");
		StringCchCopyN(wcTarget, 30, wcDeviceName, 25);

		if (lstrcmpi(wcTarget, OpticonScanner) == 0 )
		{
			OutputDebugString(TEXT("Found OPTICON!\n"));
			hBarcodeScanner = pRawInputDeviceList[i].hDevice;
		}
		if (lstrcmpi(wcTarget, SymbolScanner) == 0 )
		{
			OutputDebugString(TEXT("Found Symbol Technologies!\n"));
			hBarcodeScanner = pRawInputDeviceList[i].hDevice;
		}
		else {
			OutputDebugString(TEXT("Unable to detect Barcode Scanner Device!\n"));
		}

		// Delete Name Memory!
		delete [] wcDeviceName;
		delete [] wcTarget;
	}

	// Clean Up - Free Memory
	delete [] pRawInputDeviceList;

	return 1;
}

void cleanup(void)
{
	cfg_cleanup(&cfg);
	vlist_cleanup(&pipes);
	sock_finish();
}

int readcfg(void)
{
	char ports[BUFSIZ], *p;
	int port;
	pipe_s *pipe;
	cfg_s local;
	serialinfo_s sinfo;
	char *parity;
	char prn_name[PRINTER_NAME_SIZE];
	char net_prn_name[PRINTER_NAME_SIZE];
	char ch_log_level[LOG_LEVEL_LENGTH];

	/* Read the global config settings */
	cfg_fromfile(&cfg, cfgfile);

	/* Read the comm port list */
	if (cfg_readbuf(cfgfile, "comm_ports", ports, sizeof(ports)) == NULL) {
		OutputDebugString(TEXT("Couldn't find 'comm_ports' entry in config file...\n"));
		errend(TEXT("Couldn't find 'comm_ports' entry in config file '%s'"), cfgfile);
	}

	if (cfg_readbuf(cfgfile, "printer_name", prn_name, sizeof(prn_name)) == NULL) {
		OutputDebugString(TEXT("Couldn't find 'printer_name' entry in config file...\n"));
		errend(TEXT("Couldn't find 'printer_name' entry in config file '%s'"), cfgfile);
	}
	ConvNativeStringToTChar(prn_name, sizeof(prn_name), tchPrinterName);

	if (cfg_readbuf(cfgfile, "lan_printer", net_prn_name, sizeof(net_prn_name)) == NULL) {
		dbg_msg(TEXT("Couldn't find 'net_prn_name' entry in config file...\n"));
	}
	else {
		ConvNativeStringToTChar(net_prn_name, sizeof(net_prn_name), tchNetPrinterName);
	}
	dbg_msg(TEXT("Net printer parameter set to [%s]\n"), tchNetPrinterName);

	if (cfg_readbuf(cfgfile, "log_level", ch_log_level, sizeof(ch_log_level)) == NULL) {
		dbg_msg(TEXT("Couldn't find 'log_level' entry in config file...\n"));
		//errend(TEXT("Couldn't find 'log_level' entry in config file '%s'"), cfgfile);
	}
	else {
		ConvNativeStringToTChar(ch_log_level, sizeof(ch_log_level), szlog_level);
	}
	dbg_msg(TEXT("System 'log_level' parameter set to [%s]\n"), szlog_level);

	vlist_clear(&pipes);

	/* Parse the comm ports list */
	p = strtok(ports, ",");
	while (p)
	{
		if (sscanf(p, "%d", &port) > 0)
		{
			pipe = (pipe_s*)malloc(sizeof(pipe_s));
			//pipe_init(pipe);
			if (pipe == NULL)
				perrend(TEXT("malloc(pipe_s)"));

			cfg_init(&local, port);
			
			/* Copy global settings to those for current pipe */
			cfg_assign(&local, &cfg);

			/* Set the comm port */
			local.ints[CFG_IPORT].val = port;

			/* Load this pipe's config */
			cfg_fromfile(&local, cfgfile);

			/* Try initializing the pipe */
			if (pipe_init(pipe, local.ints[CFG_INETPORT].val))
				perrend(TEXT("pipe_init"));

			/* Copy over the rest of the pipe's config */
			pipe->timeout = local.ints[CFG_ITIMEOUT].val;
			sinfo.port = port;
			sinfo.baud = local.ints[CFG_IBAUD].val;
			sinfo.stopbits = local.ints[CFG_ISTOP].val;
			sinfo.databits = local.ints[CFG_IDATA].val;

			parity = local.strs[CFG_SPARITY].val;

			if (strcmp(parity, "none") == 0)
			{
				sinfo.parity = SIO_PARITY_NONE;
			}
			else if (strcmp(parity, "even") == 0)
			{
				sinfo.parity = SIO_PARITY_EVEN;
			}
			else if (strcmp(parity, "odd") == 0)
			{
				sinfo.parity = SIO_PARITY_ODD;
			}
			else
			{
				errend(TEXT("Unknown parity string '%s'"), parity);
			}

			if (sio_setinfo(&pipe->sio, &sinfo))
				errend(TEXT("Unable to configure comm port %d"), port);

			pipe->net_port_only = local.ints[CFG_INETPORT_ONLY].val;
			
			/* Finally add the pipe to the pipes list */
			vlist_add(&pipes, pipes.tail, pipe);

			cfg_cleanup(&local);
		}
		
		p = strtok(NULL, ",");
	}

	/* Clean up local cfg struct */
	cfg_cleanup(&local);
	
	return 0;
}

/* Main routine for the server threads */
thr_startfunc_t serve_pipe(void *data)
{
	char sio_buf[BUFSIZ], sock_buf[BUFSIZ];
	int fd_max, sio_fd, sock_fd;
	int sio_count, sock_count;
	int res, port;
	fd_set rfds, wfds;
	pipe_s *pipe = (pipe_s *)data;
#if defined(__UNIX__)
	struct timeval tv = {pipe->timeout, 0};
	struct timeval *ptv = &tv;
#elif defined(__WIN32__)
	struct timeval tv = {0,10000};
	struct timeval *ptv = &tv;
	DWORD msecs = 0, timeout = pipe->timeout * 1000;
#endif

	port = pipe->sio.info.port;

	/* Only proceed if we can lock the mutex */
	if (thr_mutex_trylock(pipe->mutex))
	{
		error(TEXT("server(%d) - resource is locked"), port);
	}
	else
	{

		sio_count = 0;
		sock_count = 0;
		sio_fd = pipe->sio.fd;
		sock_fd = pipe->sock.fd;
#if defined(__UNIX__)
		fd_max = sio_fd > sock_fd ? sio_fd : sock_fd;	
		fprintf(stderr, "server(%d) - thread started\n", port);
#elif defined(__WIN32__)
		fd_max = sock_fd;
		msecs = GetTickCount();
		dbg_msg(TEXT("Server(%d) - thread started\n"), port);
#endif

		while (1)
		{
			FD_ZERO(&rfds);
			FD_ZERO(&wfds);

#if defined(__UNIX__)
			/* Always ask for read notification to check for EOF */			
			FD_SET(sio_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sock_count > 0)
				FD_SET(sio_fd, &wfds);

			/* Reset timeout values */
			tv.tv_sec = pipe->timeout;
			tv.tv_usec = 0;

#endif
			/* Always ask for read notification to check for EOF */
			FD_SET(sock_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sio_count > 0)
				FD_SET(sock_fd, &wfds);

			//DBG_MSG2("server(%d) waiting for events", port);
			
			/* Wait for read/write events */
			res = select(fd_max + 1, &rfds, &wfds, NULL, ptv);
			if (res == -1)
			{
				perror2("server(%d) - select()", port);
				break;
			}
#if defined(__UNIX__)

			/* Use the select result for timeout detection */
			if (res == 0)
			{
				fprintf(stderr, "server(%d) - timed out\n", port);
				break;
			}

			/* Input from serial port? */
			if (FD_ISSET(sio_fd, &rfds))
#elif defined(__WIN32__)
				
			if (1)
#endif
			{
				/* Only read input if buffer is empty */
				if (sio_count == 0)
				{
					sio_count = sio_read(&pipe->sio, sio_buf, sizeof(sio_buf));
					if (sio_count <= 0)
					{
						if (sio_count == 0)
						{
#if defined(__UNIX__)
							fprintf(stderr, "server(%d) - EOF from sio\n", port);
							break;
#endif
						}
						else
						{
							perror2("server(%d) - read(sio)", port);
							break;
						}
					}
					else 
					{
						DBG_MSG3(TEXT("server(%d) - read %d bytes from sio\n"), port, sio_count);
					}
				}
			}

			/* Write to socket possible? */
			if (FD_ISSET(sock_fd, &wfds))
			{
				if (sio_count > 0)
				{
					if ((res = tcp_write(&pipe->sock, sio_buf, sio_count)) < 0)
					{
						perror2("server(%d) - write(sock)", port);
						break;
					}
					DBG_MSG3(TEXT("server(%d) - Wrote %d bytes to sock\n"), port, res);
					sio_count -= res;
				}
			}


			/* Input from socket? */
			if (FD_ISSET(sock_fd, &rfds))
			{
				/* Only read input if buffer is empty */
				if (sock_count == 0)
				{
					sock_count = tcp_read(&pipe->sock, sock_buf, sizeof(sock_buf));
					if (sock_count <= 0)
					{
						if (sock_count == 0)
						{
#if defined (__UNIX__)
							fprintf(stderr, "server(%d) - EOF from sock\n", port);
#elif defined (__WIN32__)
							dbg_msg(TEXT("server(%d) - EOF from sock\n"), port);
#endif
							break;
						}
						else
						{
							perror2("server(%d) - read(sock)", port);
							break;
						}
					}
					DBG_MSG3(TEXT("server(%d) - read %d bytes from sock\n"), port, sock_count);
				}
			}

#if defined(__UNIX__)
			/* Write to serial port possible? */
			if (FD_ISSET(sio_fd, &wfds))
#elif defined(__WIN32__)
			
			/* No socket IO performed? */
			if ((!FD_ISSET(sock_fd, &rfds)) && (!FD_ISSET(sock_fd, &wfds)))
			{
				/* Break on a time out */
				if (GetTickCount() - msecs > timeout)
				{
					fprintf(stderr, "server(%d) - timed out\n", port);
					break;					
				}
			}
			else
			{
				msecs = GetTickCount();
			}

			if (1)
#endif
			{
				if (sock_count > 0)
				{
					if ((res = sio_write(&pipe->sio, sock_buf, sock_count)) < 0)
					{
						perror2("server(%d) - write(sio)", port);
						break;
					}
					DBG_MSG3(TEXT("server(%d) - wrote %d bytes to sio\n"), port, res);
					sock_count -= res;
				}
			}

		}  /* End of while(1) loop */
		
		/* Unlock our mutex */
		thr_mutex_unlock(pipe->mutex);		
	}

#if defined (__UNIX__)
   	fprintf(stderr, "server(%d) exiting\n", port);
#elif defined (__WIN32__)
	dbg_msg(TEXT("server(%d) exiting\n"), port);
#endif
	/* Clean up - don't call pipe_cleanup() as that would nuke our mutex */
	sio_cleanup(&pipe->sio);
	tcp_cleanup(&pipe->sock);


	free(pipe);
	
	thr_exit((thr_exitcode_t)0);

	return (thr_exitcode_t)0;
}

/* Main routine for the server threads */
/* EXTENDED VERSION                    */
thr_startfunc_t serve_pipe_ext(void *data)
{
	char sio_buf[BUFSIZ], sock_buf[BUFSIZ];
	int fd_max, sio_fd, sock_fd;
	int sio_count, sock_count;
	int res, port;
	fd_set rfds, wfds;
	pipe_s *pipe = (pipe_s *)data;
#if defined(__UNIX__)
	struct timeval tv = {pipe->timeout, 0};
	struct timeval *ptv = &tv;
#elif defined(__WIN32__)
	struct timeval tv = {0,10000};
	struct timeval *ptv = &tv;
	DWORD msecs = 0, timeout = pipe->timeout * 1000;
	DWORD tickCnt;
#endif

	port = pipe->sio.info.port;

	/* Only proceed if we can lock the mutex */
	if (thr_mutex_trylock(pipe->mutex))
	{
		error(TEXT("server(%d) - resource is locked"), port);
	}
	else
	{

		sio_count = 0;
		sock_count = 0;
		sio_fd = pipe->sio.fd;
		sock_fd = pipe->sock.fd;
#if defined(__UNIX__)
		fd_max = sio_fd > sock_fd ? sio_fd : sock_fd;	
		fprintf(stderr, "server(%d) - thread started\n", port);
#elif defined(__WIN32__)
		fd_max = sock_fd;
		msecs = GetTickCount();
		dbg_msg(TEXT("Server(%d) - thread started\n"), port);
#endif

		while (1)
		{
			FD_ZERO(&rfds);
			FD_ZERO(&wfds);

#if defined(__UNIX__)
			/* Always ask for read notification to check for EOF */			
			FD_SET(sio_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sock_count > 0)
				FD_SET(sio_fd, &wfds);

			/* Reset timeout values */
			tv.tv_sec = pipe->timeout;
			tv.tv_usec = 0;

#endif
			if (nReturnSocketRAW > 0) {
				sio_count = nReturnSocketRAW;
				memcpy(sio_buf, g_szReturnSocketRAW, nReturnSocketRAW);
				nReturnSocketRAW = 0;
			}

			/* Always ask for read notification to check for EOF */
			FD_SET(sock_fd, &rfds);
			/* Only ask for write notification if we have something to write */
			if (sio_count > 0)
				FD_SET(sock_fd, &wfds);

			//DBG_MSG2("server(%d) waiting for events", port);
			
			/* Wait for read/write events */
			res = select(fd_max + 1, &rfds, &wfds, NULL, ptv);
			if (res == -1)
			{
				perror2("server(%d) - select()", port);
				break;
			}
#if defined(__UNIX__)

			/* Use the select result for timeout detection */
			if (res == 0)
			{
				fprintf(stderr, "server(%d) - timed out\n", port);
				break;
			}

			/* Input from serial port? */
			if (FD_ISSET(sio_fd, &rfds))
#elif defined(__WIN32__)
				
			if (1)
#endif
			{
				/* Only read input if buffer is empty */
				if (sio_count == 0)
				{
					sio_count = 0;
					if (!pipe->net_port_only)
						sio_count = sio_read(&pipe->sio, sio_buf, sizeof(sio_buf));

					if (sio_count <= 0)
					{
						if (sio_count == 0)
						{
#if defined(__UNIX__)
							fprintf(stderr, "server(%d) - EOF from sio\n", port);
							break;
#endif
						}
						else
						{
							perror2("server(%d) - read(sio)", port);
							break;
						}
					}
					else 
					{
						DBG_MSG3(TEXT("server(%d) - read %d bytes from sio\n"), port, sio_count);
					}
				}
			}

			/* Write to socket possible? */
			if (FD_ISSET(sock_fd, &wfds))
			{
				if (sio_count > 0)
				{
					if ((res = tcp_write(&pipe->sock, sio_buf, sio_count)) < 0)
					{
						perror2("server(%d) - write(sock)", port);
						break;
					}
					DBG_MSG3(TEXT("server(%d) - Wrote %d bytes to sock\n"), port, res);
					sio_count -= res;
				}
			}


			/* Input from socket? */
			if (FD_ISSET(sock_fd, &rfds))
			{
				/* Only read input if buffer is empty */
				if (sock_count == 0)
				{
					dbg_msg(TEXT("serve_pipe pre-send WM_CHK_SOCKINPUT!\n"));
					SendMessage(hWndPOSAssist, WM_CHK_SOCKINPUT, sock_fd, 0);

					dbg_msg(TEXT("serve_pipe finished to send WM_Message!\n"));
					sock_count = nChSockRAW;
					//pchana set char[] to null
					memset(sock_buf, 0, sizeof(sock_buf));
					memcpy(sock_buf, g_szSocketRAW, nChSockRAW);

					if ((sock_count <= 0) &&
						(!isCMDSockRAW))
					{
						if (sock_count == 0)
						{
#if defined (__UNIX__)
							fprintf(stderr, "server(%d) - EOF from sock\n", port);
#elif defined (__WIN32__)
							dbg_msg(TEXT("server(%d) - EOF from sock\n"), port);
#endif
							break;
						}
						else
						{
							perror2("server(%d) - read(sock)", port);
							break;
						}
					}
					DBG_MSG3(TEXT("server(%d) - read %d bytes from sock\n"), port, sock_count);
				}
			}

#if defined(__UNIX__)
			/* Write to serial port possible? */
			if (FD_ISSET(sio_fd, &wfds))
#elif defined(__WIN32__)
			tickCnt = GetTickCount();

			/* No socket IO performed? */
			if ((!FD_ISSET(sock_fd, &rfds)) && (!FD_ISSET(sock_fd, &wfds)))
			{
				/* Break on a time out */
				if (tickCnt - msecs > timeout)
				{
					fprintf(stderr, "server(%d) - timed out\n", port);
					/**
					 * Connection will be closed if no socket IO performed
					 * within 5 minutes.
					 **/
					dbg_msg(TEXT("server(%d) - timed out\n"), port);
					break;
				}
			}
			else
			{
				msecs = tickCnt;
			}

			if (1)
#endif
			{
				if (sock_count > 0)
				{
					if (!pipe->net_port_only) {
						if ((res = sio_write(&pipe->sio, sock_buf, sock_count)) < 0)
						{
							perror2("server(%d) - write(sio)", port);
							break;
						}
						DBG_MSG3(TEXT("server(%d) - wrote %d bytes to sio\n"), port, res);
					}
					else {
						//pchana do
						//mock-up data
						//string datamockup[3][2] = {
							//{"0","AAA"},
							//{"1","BBB"},
							//{"2","CCC"}
						//};
						//char a[sizeof(sock_buf)];
						int size = 0;
						for (int i = 0; i < sizeof(sock_buf); i++) {
							if ((int)sock_buf[i] == (int)'_'
								|| ((int)sock_buf[i] >= (int)'0' && (int)sock_buf[i] <= (int)'9')
								|| ((int)sock_buf[i] >= (int)'A' && (int)sock_buf[i] <= (int)'Z')
								|| ((int)sock_buf[i] >= (int)'a' && (int)sock_buf[i] <= (int)'z')
								|| (int)sock_buf[i] == (int)',') {//,
								size++;
							}
						}
						string str = "";
						for (int i = 0; i < size; i++) {
							str += sock_buf[i];
						}
						size_t indexCut = str.find(",");
						string comand = str.substr(0, indexCut);
						string val = str.substr(indexCut + 1);

						string strReturn;
						if (val.find(",") != string::npos) {
							indexCut = val.find(",");
							string val2 = val.substr(indexCut + 1);
							val = val.substr(0, indexCut);

							if (comand == "cmd_w") {
								//can use map
								datamockup[stoi(val)][1] = val2;
								strReturn = datamockup[stoi(val)][1] + "update sussess";
								char cstrReturn[sizeof(strReturn)];
								strcpy(cstrReturn, strReturn.c_str());
								if (sock_count > 0)
								{
									if ((res = tcp_write(&pipe->sock, cstrReturn, sizeof(cstrReturn))) < 0)
									{
										perror2("server(%d) - write(sock)", port);
										break;
									}
									DBG_MSG3(TEXT("server(%d) - Wrote %d bytes to sock\n"), port, res);
									sock_count = 0;
								}
							}
						}
						else {
							if (comand == "cmd_r") {
								//can use map
								strReturn = datamockup[stoi(val)][1];
								char cstrReturn[sizeof(strReturn)];
								strcpy(cstrReturn, strReturn.c_str());
								if (sock_count > 0)
								{
									if ((res = tcp_write(&pipe->sock, cstrReturn, sizeof(cstrReturn))) < 0)
									{
										perror2("server(%d) - write(sock)", port);
										break;
									}
									DBG_MSG3(TEXT("server(%d) - Wrote %d bytes to sock\n"), port, res);
									sock_count = 0;
								}
							}
						}
					}
				}
			}

		}  /* End of while(1) loop */
		
		/* Unlock our mutex */
		thr_mutex_unlock(pipe->mutex);		
	}

#if defined (__UNIX__)
   	fprintf(stderr, "server(%d) exiting\n", port);
#elif defined (__WIN32__)
	dbg_msg(TEXT("server(%d) exiting\n"), port);
#endif
	/* Clean up - don't call pipe_cleanup() as that would nuke our mutex */
	sio_cleanup(&pipe->sio);
	tcp_cleanup(&pipe->sock);


	free(pipe);
	
	thr_exit((thr_exitcode_t)0);

	return (thr_exitcode_t)0;
}

void debug(void)
{
	vlist_i *it;
	pipe_s *pit;
	int i = 1;
	
	fprintf(stderr, "pipes:\n\n");
	vlist_debug(&pipes, stderr);

	for (it = pipes.head; it; it = it->next)
	{
		pit = (pipe_s *)it->data;

		fprintf(stderr, "sio[%d]:\n\n", i);
		sio_debug(&pit->sio, stderr);

		fprintf(stderr, "sock[%d]:\n\n", i);
		tcp_debug(&pit->sock, stderr);

		i++;
	}
}

int preWaitClients()
{
	if (sock_start()) {
		OutputDebugString(TEXT("Error: Sock_start()\n"));
		return -1;
	}
	vlist_init(&pipes, pipe_destroy);
	cfg_init(&cfg, 0);
	atexit(cleanup);
	readcfg();
#ifdef DEBUG
	debug();
#endif

/// Previously all below statements are from waitclients() /////
	vlist_i *it;
	pipe_s *pit;
	int nResult;

	/* Set all sockets to listen */
	for (it = pipes.head; it; it = it->next)
	{
		pit = (pipe_s *)it->data;

		nResult = WSAAsyncSelect(pit->sock.fd,
					hWndPOSAssist,
					WM_POSASSIST_SOCKET,
					(FD_CLOSE|FD_ACCEPT|FD_READ|FD_WRITE));
		if (nResult) {
				MessageBox(hWndPOSAssist,
					TEXT("WSAAsyncSelect failed"),
					TEXT("Critical Error"),
					MB_ICONERROR);
				SendMessage(hWndPOSAssist,WM_DESTROY,NULL,NULL);
				return -2;
		}

		if (tcp_listen(&pit->sock))
			perror("waitclients() - tcp_listen()");
	}

	OutputDebugString(TEXT("POSAssist - Waiting for clients\n"));
	return 0;
}

int	findInvolvedSocket(SOCKET reqSocket)
{
	vlist_i		*it;
	pipe_s		*pit, *newpipe;
	tcpsock_s	*newsock;
	thr_t		thread;

	/*** Find which sockets are involved */
	for (it = pipes.head; it; it = it->next) {
		pit = (pipe_s *)it->data;

		/*** Replaced FD_ISSET() with WSAAsyncSelect() */
		if (reqSocket == pit->sock.fd) {
			/* Create a new pipe struct for the new thread */
			newpipe = (pipe_s *)malloc(sizeof(pipe_s));
			if (!newpipe)
				perrend(TEXT("waitclients() - malloc(pipe_s)"));

			newpipe->sio = pit->sio;

			if (!pit->net_port_only) {
				/* Try to open serial port */
				if (sio_open(&newpipe->sio) && (!pit->net_port_only)) {
					tcp_refuse(&pit->sock);
					error(TEXT("Failed to open comm port - connection refused\n"));
					free(newpipe);
					continue;
				}
			}

			/* Accept the connection */
			newsock = tcp_accept(&pit->sock);

			/* All ok? */
			if (newsock) {
				newpipe->sock = *newsock;
				free(newsock);

				newpipe->timeout = pit->timeout;
				newpipe->mutex = pit->mutex;
				newpipe->net_port_only = pit->net_port_only;

				/** Store Serial Com info to current pipe */
				/** pit->sio.fd    = newpipe->sio.fd; <-- DO NOT store the "fd"
				 *  since it will nuke the application when system closed. */
				pit->sio.hComm = newpipe->sio.hComm;

				/* Create the server thread */
				int iRes;
				if ((POSASSIST_EXT_PORT == pit->sock.port)||
					(pit->net_port_only)) {
					DBG_MSG1(TEXT("serve_pipe_EXT Thread create...\n"));
					if (iRes = thr_create(&thread, 1, serve_pipe_ext, newpipe)) {
						error(TEXT("Error - thread creation failed"));
						free(newpipe);
					}
				}
				else {
					DBG_MSG1(TEXT("serve_pipe Thread create...\n"));
					if (iRes = thr_create(&thread, 1, serve_pipe, newpipe)) {
						error(TEXT("Error - thread creation failed"));
						free(newpipe);
					}
				}

				if (!iRes) {
#if defined(__UNIX__)
					fprintf(stderr, "Server thread launched\n");
#elif defined(WIN32)
					dbg_msg(TEXT("Server launched thread Handler: (0x%0X)\n"), thread);
#endif
				}
			}
			else {
				perror("waitclients() - accept()");
				free(newpipe);
			}
		}
	}  /*** Find which sockets are involved */

	return newpipe->sock.fd;
}

/* Filter Command or Data in Socket Buffer */
INT	FilterSocketRAW(SOCKET callSocket)
{
	HRESULT hResult;
	WCHAR szCMDBuff[SOCKETRAW_BUFFER_SIZE];
	ZeroMemory(g_szSocketRAW, sizeof(g_szSocketRAW));
	nChSockRAW = 0;

	int inDataLength = recv(callSocket,
							(char*)g_szSocketRAW,
							sizeof(g_szSocketRAW),
							0);

	if ((inDataLength >= MIN_CMD_LENGTH) &&
		(CMD_HEADER == g_szSocketRAW[0]))
	{
		hResult = StringCchPrintf(szCMDBuff,
								  SOCKETRAW_BUFFER_SIZE,
								  TEXT("%S"),
								  &g_szSocketRAW[1]);
		if (FAILED(hResult))
			OutputDebugString (TEXT("StringCchPrintf does not return correct size! \n"));

		dbg_msg(TEXT("FilterSocketRAW TRAP CMD:: %s \n"), szCMDBuff);

		ZeroMemory(g_szSocketRAW, sizeof(g_szSocketRAW));
		nChSockRAW = 0;

		if (0 != OperateNetCMD(szCMDBuff)) {
			dbg_msg(TEXT("OperateNetCMD return result.\n"));
		}

		return 1;
	}
	else if ((inDataLength >= MIN_CMD_LENGTH) &&
			 (CMD_UTF_HEADER == g_szSocketRAW[0]))
	{
		WCHAR szCMDBuffDest[512];
		WCHAR tchBuff[SOCKETRAW_BUFFER_SIZE];

		wmemcpy(tchBuff, (wchar_t*)g_szSocketRAW, SOCKETRAW_BUFFER_SIZE);
		hResult = StringCchPrintf(szCMDBuff,
								  SOCKETRAW_BUFFER_SIZE,
								  TEXT("%s"),
								  &tchBuff[1]);
		if (FAILED(hResult))
			OutputDebugString (TEXT("StringCchPrintf does not return correct size! \n"));

		dbg_msg(TEXT("FilterSocketRAW TRAP UTF CMD:: %s \n"), szCMDBuff);

		ZeroMemory(g_szSocketRAW, sizeof(g_szSocketRAW));
		nChSockRAW = 0;

		if (0 != OperateNetCMD(szCMDBuff)) {
			dbg_msg(TEXT("OperateNetCMD return result.\n"));
		}

		ZeroMemory(tchBuff, sizeof(tchBuff));
		return 2;
	}
	else {
		nChSockRAW = inDataLength;
		dbg_msg(TEXT("FilterSocketRAW FORWARD data.\n"));
	}

	return 0;
}

/* Operate Net Request CMD */
INT	OperateNetCMD(WCHAR *pCMDBuff)
{
	HRESULT	hResult;
	size_t  lenStr;

	INT iCMDResult   = 0;
	nReturnSocketRAW = 0;

	if (0 == lstrcmpi(CMD_GET_BSCLINE, pCMDBuff)) {
		/**
		if (!strcmp((const char *)TEXT("REQCMD"), (const char *)szCMDBuff))
		can be used also.
		 */

		dbg_msg(TEXT("OperateNetCMD: CMD_GET_BSCLINE.\n"));

		hResult = StringCchLength(szBSCFIFO, (size_t)MAX_LOADSTRING, &lenStr);
		if (lenStr > 0) {
			sprintf_s(g_szReturnSocketRAW, "%S %S",
				STATUS_OK,
				szBSCFIFO);
			nReturnSocketRAW = strlen(g_szReturnSocketRAW);
			iCMDResult = 0;

			/** Clean up szScanData */
			hResult = StringCchPrintf(szBSCFIFO,
									MAX_LOADSTRING,
									TEXT(""));
			if (FAILED(hResult))
				OutputDebugString (TEXT("Can not clean up szBSCFIFO.\n"));
		}
		else {
			hResult = StringCchLength(szScanData, (size_t)MAX_LOADSTRING, &lenStr);
			if (lenStr > 0) {
				sprintf_s(g_szReturnSocketRAW, "%S %S",
					STATUS_NG,
					FIFO_BUSY);
			}
			else {
				sprintf_s(g_szReturnSocketRAW, "%S %S",
					STATUS_NG,
					FIFO_EMPTY);
			}

			nReturnSocketRAW = strlen(g_szReturnSocketRAW);
			iCMDResult = 0;
		}
	}
	else if (0 == lstrcmpi(CMD_UNLOCK_CADR, pCMDBuff)) {
		/**
		 * Let serve pipe to send UNLOCK_BYTE for unlocking
		 * the cash drawer.
		 */
		dbg_msg(TEXT("OperateNetCMD: CMD_UNLOCK_CADR.\n"));

		g_szSocketRAW[0] = UNLOCK_BYTE;
		nChSockRAW       = 1;

		iCMDResult = 0;
	}
	else if (0 == lstrcmpi(CMD_FORCE_PRINT, pCMDBuff)) {
		/**
		 * [Work Arround] to force system to print slipt out.
		 * By finding "Print" dialog box and send window message.
		 */
		dbg_msg(TEXT("OperateNetCMD: CMD_FORCE_PRINT.\n"));
		::Sleep(440); /** Wait for "Print" dialog box created. */
		dbg_msg(TEXT("---->: CMD_FORCE_PRINT Start HOOK!.\n"));

	    HWND hDlg;
	    hDlg = FindWindow(NULL, TEXT("Print"));
	    if (NULL != hDlg) {
		    /** AutomationId = 1 (OK Button) */
		    SendDlgItemMessage(hDlg, 1, WM_LBUTTONDOWN, NULL, NULL);
		    SendDlgItemMessage(hDlg, 1, WM_LBUTTONUP,   NULL, NULL);
			dbg_msg(TEXT("---->: CMD_FORCE_PRINT HOOK Done!!!.\n"));
	    }

	    iCMDResult = 0;
	}
	else if (0 == lstrcmpi(CMD_GET_CADRSTAT, pCMDBuff)) {
		dbg_msg(TEXT("OperateNetCMD: CMD_GET_CADRSTAT.\n"));

		pipe_s *pit = FindPipe(POSASSIST_EXT_PORT);
		if ((NULL != pit) &&
			(INVALID_HANDLE_VALUE != pit->sio.hComm))  // [Rev.] It should be serve_pipe_ext & pit->sio.hComm valid.
		{
			DWORD dwStatus;
			if (GetCommModemStatus(pit->sio.hComm, &dwStatus)) {
				if (dwStatus & MS_RLSD_ON) {
					dbg_msg(TEXT("The RLSD(CD) Signal [ON] - Cash Drawer is [[OPENED]].\n"));
					sprintf_s(g_szReturnSocketRAW, "%S %S",
								STATUS_OK,
								CADR_OPEN);
				}
				else {
					dbg_msg(TEXT("The RLSD(CD) Signal [OFF] - Cash Drawer is [[CLOSED]].\n"));
					sprintf_s(g_szReturnSocketRAW, "%S %S",
								STATUS_OK,
								CADR_CLOSE);
				}

				nReturnSocketRAW = strlen(g_szReturnSocketRAW);
				iCMDResult = 0;
			}
			else {
				/** NEED to verify fail check */
				dbg_msg(TEXT("Get COMM Modem Status FAIL!\n"));
				iCMDResult = 1;
			}
		}
		else {
			/** NEED to verify fail check */
			dbg_msg(TEXT("Pipe ERROR or COMM Handler FAIL!\n"));
			iCMDResult = 1;
		}
	}
	else if (lstrcmp(pCMDBuff, CMD_RFID_INIT_SL500) == 0) {
		dbg_msg(TEXT("OperateNetCMD: CMD_RFID_INIT_SL500\n"));
		//sio_cleanup(&pipe->sio);

		sl500_init();
		iCMDResult = 0;
	}
	else if (lstrcmp(pCMDBuff, CMD_RFID_CLOSE_SL500) == 0) {
		dbg_msg(TEXT("OperateNetCMD: CMD_RFID_CLOSE_SL500\n"));
		sl500_close();
		iCMDResult = 0;
	}
	else if (lstrcmp(pCMDBuff, CMD_RFID_REQ_CARD_ID_SL500) == 0) {
		dbg_msg(TEXT("OperateNetCMD: CMD_RFID_REQ_CARD_ID_SL500\n"));
		// sl500_req_card_ID();
		Operate_RFID_Read();
		iCMDResult = 0;
	}
	else if (lstrcmp(pCMDBuff, CMD_PRINT_LABEL) >= 0)
	{
		int cnt = lstrcmp(pCMDBuff, CMD_PRINT_LABEL);

		/** 
			NEED to FIX (1)
		**/

		//dbg_msg(TEXT("OperateNetCMD: CMD_PRINT_LABEL [%s] - Count[%n]\n"), pCMDBuff, cnt);
		// ^^^ above statement generate BUG access violation.
		//
		dbg_msg(TEXT("OperateNetCMD: CMD_PRINT_LABEL\n"));
		// ^^^ So we use this temporary instead.

		__try
        {
			/** 
			NEED to FIX (2)
			To DEBUG: the bulk message.
			    Set BreakPoint Here!!
			**/

			/**
				Send all text sequence to DirectPrintLabel()
				+ 1 : Mean to skip the first '\n' character
			**/
			iCMDResult = DirectPrintLabel(&pCMDBuff[lstrlen(CMD_PRINT_LABEL) + 1]);
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
        {
			/**
				Idea to do the TRY EXECPTION is from:
				http://support.microsoft.com/kb/315937
			**/
			dbg_msg(TEXT("EXCEPTION_EXECUTE_HANDLER // OperateNetCMD: CMD_PRINT_LABEL ^^^^^ \n"));
		}
	}
	else if (lstrcmp(pCMDBuff, CMD_KITCHEN_PRINT) >= 0) {
		int cnt = lstrcmp(pCMDBuff, CMD_KITCHEN_PRINT);
		dbg_msg(TEXT("OperateNetCMD: CMD_KITCHEN_PRINT\n"));

		__try
        {
			iCMDResult = DirectPrintOrderList(&pCMDBuff[lstrlen(CMD_KITCHEN_PRINT) + 1]);
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
        {
			dbg_msg(TEXT("EXCEPTION_EXECUTE_HANDLER // OperateNetCMD: CMD_KITCHEN_PRINT ^^^^^ \n"));
		}
	}
	else {
		dbg_msg(TEXT("OperateNetCMD: Not found spacific command.\n"));
		sprintf_s(g_szReturnSocketRAW, "%S %S",
			STATUS_NG,
			UNKNOWN_MSG);
		nReturnSocketRAW = strlen(g_szReturnSocketRAW);

		iCMDResult = 0;
	}

	return iCMDResult;
}


LRESULT	CALLBACK SystemTrayAct(LPARAM lParam)
{
	switch (lParam) {
	case WM_LBUTTONDBLCLK:
		dbg_msg(TEXT("System Tray was DBL Click !\n"));
		HideWindow();
		break;
	default:
		//dbg_msg(TEXT("UNKOWN System tray message request.\n"));
		break;
	}

	return 0;
}


VOID HideWindow()
{
	if (bHideWnd) {
		::ShowWindow(hWndPOSAssist, SW_SHOW);
	}
	else {
		::ShowWindow(hWndPOSAssist, SW_HIDE);
	}

	bHideWnd = !bHideWnd;
}


pipe_s* FindPipe(int port)
{
	vlist_i	*it;
	pipe_s	*pit;

	for (it = pipes.head; it; it = it->next) {
		pit = (pipe_s *)it->data;

		if (port == pit->sock.port)
			return pit;
	}

	return NULL; /** Not Found */
}


INT	ConvNativeStringToTChar(char *pchSrc, size_t szSrc, TCHAR *pTarget)
{
	/** Adapted from "How to: Convert Between Various String Types"
		http://msdn.microsoft.com/en-us/library/ms235631(v=vs.80).aspx
	**/

	// Convert to a wchar_t*
	char *orig = pchSrc;
	size_t origsize = szSrc;
    const size_t newsize = szSrc;
    size_t convertedChars = 0;

	mbstowcs_s(&convertedChars, pTarget, origsize, orig, _TRUNCATE);
	return 0;
}

#define MAX_NUM_TEXTBLK		 9
#define MAX_LENGTH_TEXTBLK	30

INT	DirectPrintLabel(WCHAR *pTextCmdBuff)
{
	static HANDLE HdevMode;
	HDC prn;
	HDC hdcMem;
	HBITMAP hBitmap;

	TCHAR * text = TEXT("Hello");
	TCHAR * itemNameEng = TEXT("Jasmine Milk Green Tea");
	TCHAR * Time = TEXT("09:25");
	TCHAR * Date = TEXT("20/05/2555");
	TCHAR * salePrice = TEXT("999B");
	TCHAR * saleQTY = TEXT("1000 - 1000");
	TCHAR * salestock = TEXT("1");
	TCHAR * itemOption = TEXT("GJ/Less Ice/100%S");
	TCHAR * itemOption2 = TEXT("NOICE");
	//TCHAR * itemNamethai = TEXT("�ҹ����� JMGT");
	TCHAR * itemNamethai = pTextCmdBuff;
	TCHAR * itemNameCode = TEXT("JMGT");
	TCHAR * BillCount = TEXT("001");
	TCHAR * phoneNumber = TEXT("087 589 6886 - 087 589 6886");

	HFONT hFont;
	HFONT hFont20;
	HFONT hFont30;
	HFONT hFont50;
	HFONT hFontNameThai;
	HFONT hFontNameEng;

	int cxpage = 0;
	int cypage = 0;

	int count = 1;

	DOCINFO di= { sizeof(DOCINFO), TEXT("Sticker") };
	TCHAR prnText[MAX_NUM_TEXTBLK][MAX_LENGTH_TEXTBLK];

	int j=0;
	int h=0;
	for (int i=0; i < lstrlen(pTextCmdBuff); i++) {
		if (j > MAX_NUM_TEXTBLK)
			continue;

		if ('\\' == pTextCmdBuff[i]) {
			StringCchCopyN(prnText[j], sizeof(prnText[j]),
				&pTextCmdBuff[h], i-h);

			dbg_msg(TEXT("Found text[%d] : %s\n"), lstrlen(prnText[j]), prnText[j]);
			j++;
			h=i+1;
		}
	}

	/** Ordering Assignment **/
	BillCount = prnText[0]; //[1]
	saleQTY = prnText[1]; //[2]
	salestock = TEXT("");

	itemNamethai = prnText[2]; //[3]
	itemNameCode = TEXT("");
	itemNameEng = prnText[3]; //[4]

	itemOption = prnText[4]; //[5]
	itemOption2 = TEXT("");

	Date = prnText[5]; //[6]
	Time = prnText[6]; //[7]
	salePrice = prnText[7]; //[8]
	phoneNumber = prnText[8];
	/******************************/

	// change printer to change name "doPDF v7" -> "printer name" | start -> Devices and PRinters
	//prn = CreateDC(L"WINSPOOL",L"Brother QL-720NW USB",NULL,NULL); 
	//prn = CreateDC(L"WINSPOOL",L"doPDF v7",NULL,NULL);
	//prn = CreateDC(L"WINSPOOL",L"EPSON TM-T88IV Receipt",NULL,NULL); 
	//prn = CreateDC(L"WINSPOOL",L"Birch BP-525D",NULL,NULL);

	dbg_msg(TEXT("Set PRN_NAME: %s\n"), tchPrinterName);
	prn = CreateDC(L"WINSPOOL", tchPrinterName, NULL, NULL);

	// Default to landscape
	if (HdevMode == NULL)
	{
		// DevMode structure needs to be allocated
		HdevMode = GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, sizeof(DEVMODE));
		ZeroMemory(HdevMode, sizeof(DEVMODE)); // GMEM_ZEROINIT should have initialized all of this to zero, but it did not
		DEVMODE* dm = (DEVMODE*)HdevMode;
		dm->dmSize = sizeof(DEVMODE);
	}
	else if (HdevMode != NULL)
	{
		// Set to landscape orientation
		DEVMODE* dm = (DEVMODE*)HdevMode;
		dm->dmFields = DM_ORIENTATION;
		dm->dmOrientation = DMORIENT_LANDSCAPE;
	}

	// Initialize PRINTDLG
	PRINTDLG pd = {0};
	ZeroMemory(&pd, sizeof(PRINTDLG));
	pd.lStructSize = sizeof(PRINTDLG);
	pd.hwndOwner   = hWndPOSAssist; //hwnd;
	pd.hDevMode    = HdevMode;      // Don't forget to free or store hDevMode.
	pd.Flags       = PD_RETURNDC | PD_NOPAGENUMS | PD_USEDEVMODECOPIESANDCOLLATE | PD_NOSELECTION;
	pd.nCopies     = 1;
	pd.nFromPage	= 0xFFFF;
	pd.nToPage		= 0xFFFF;
	pd.nMinPage		= 1;
	pd.nMaxPage		= 0xFFFF;

	cxpage = GetDeviceCaps (prn, HORZRES);
	cypage = GetDeviceCaps (prn, VERTRES);
	hdcMem = CreateCompatibleDC(prn);

	StartDoc (prn, &di);
	StartPage (prn);
	SetMapMode (prn, MM_ISOTROPIC);
	SetWindowExtEx(prn, cxpage,cypage, NULL);
	SetViewportExtEx(prn, cxpage, cypage,NULL);

	SetViewportOrgEx(prn, 0, 0, NULL);
	StretchBlt(prn, 0, 0, cxpage, cypage, hdcMem, 0, 0,412,313, SRCCOPY);

	hFont = CreateFont(40,0,0,0,FW_BOLD ,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont);
	TextOut(prn,   7,  12, BillCount, lstrlen(BillCount));
	DeleteObject(hFont);  

	hFont20 = CreateFont(30,0,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont20);
	TextOut(prn,   7, 140,       Date, lstrlen(Date));
	TextOut(prn, 147, 140,       Time, lstrlen(Time));
	TextOut(prn,   7, 110, itemOption, lstrlen(itemOption));
	TextOut(prn,  47, 167, phoneNumber, lstrlen(phoneNumber));
	DeleteObject(hFont20);

	hFont30 = CreateFont(30,0,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont30);
	DeleteObject(hFont30);   

	hFont50 = CreateFont(36,0,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont50);
//	TextOut(prn, 215, 12,   saleQTY, lstrlen(saleQTY));
	TextOut(prn, 275, 12, salestock, lstrlen(salestock));
	DeleteObject(hFont50);  

	hFontNameThai = CreateFont(36,0,0,0,FW_DONTCARE,FALSE,TRUE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, TEXT("Arial"));
	SelectObject(prn, hFontNameThai);
	TextOut(prn, 7, 45, itemNamethai, lstrlen(itemNamethai));
	DeleteObject(hFontNameThai);

	hFontNameEng = CreateFont(30,0,0,0,FW_DONTCARE,FALSE,TRUE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
	CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFontNameEng);
	TextOut(prn, 7, 80, itemNameEng, lstrlen(itemNameEng));
	DeleteObject(hFontNameEng);

	SetTextAlign(prn,TA_RIGHT);
	hFont50 = CreateFont(36,0,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
						CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont50);
	TextOut(prn , 305 , 12 ,saleQTY,lstrlen(saleQTY));
	//TextOut(prn , 275, 12 ,salestock ,lstrlen(salestock));
	//TextOut(prn , 60 , 55 ,itemNamethai,lstrlen(itemNamethai));
	DeleteObject(hFont50);

	hFont30 = CreateFont(40,0,0,0,FW_BOLD,FALSE,FALSE,FALSE,THAI_CHARSET,OUT_CHARACTER_PRECIS,
						CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("Arial"));
	SelectObject(prn, hFont30);
	TextOut(prn , 305 , 132 ,salePrice,lstrlen(salePrice));
	DeleteObject(hFont30);

	EndPage (prn);
	EndDoc(prn);
	DeleteDC(prn);
	DeleteDC(hdcMem);

	return 0;
}


#define MAX_NUM_TEXTBLK_ORDER		MAX_NUM_TEXTBLK + 50
#define MAX_LENGTH_TEXTBLK_ORDER	MAX_LENGTH_TEXTBLK + 100

INT	DirectPrintOrderList(WCHAR *pTextCmdBuff)
{
	static HANDLE HdevMode;
	HDC prn;
	HDC hdcMem;
	HBITMAP hBitmap;

	TCHAR * text = TEXT("Hello");
	TCHAR * prnTestText = TEXT("*** TEST TEST ���ͺ�к� ***");
	TCHAR * line_string = TEXT("----------------------------------- -----------------------------------");
	TCHAR * BillCount = TEXT("Naa Shoooo 001");

	HFONT hFont;
	HFONT hFont20;
	HFONT hFont30;
	HFONT hFont50;
	HFONT hFontNameThai;
	HFONT hFontNameEng;

	int cxpage = 0;
	int cypage = 0;

	int count = 1;

	DOCINFO di= { sizeof(DOCINFO), TEXT("KitchenPrint") };
	TCHAR prnText[MAX_NUM_TEXTBLK_ORDER][MAX_LENGTH_TEXTBLK_ORDER];

	int j=0;
	int h=0;
	for (int i=0; i < lstrlen(pTextCmdBuff); i++) {
		if (j > MAX_NUM_TEXTBLK_ORDER)
			continue;

		if ('\\' == pTextCmdBuff[i]) {
			StringCchCopyN(prnText[j], sizeof(prnText[j]),
				&pTextCmdBuff[h], i-h);

			dbg_msg(TEXT("Found text[%d] : %s\n"), lstrlen(prnText[j]), prnText[j]);
			j++;
			h=i+1;
		}
	}

	int _prn_lines = j; //j + 1;
	dbg_msg(TEXT("Gonna Print Lines: %d\n"), _prn_lines);
	dbg_msg(TEXT("Set PRN_NAME: %s\n"), tchNetPrinterName);
	prn = CreateDC(L"WINSPOOL", tchNetPrinterName, NULL, NULL);

// == Start: Printing Prepare Session =======================================================

	// Default to landscape
	if (HdevMode == NULL)
	{
		// DevMode structure needs to be allocated
		HdevMode = GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, sizeof(DEVMODE));
		// GMEM_ZEROINIT should have initialized all of this to zero, but it did not

		ZeroMemory(HdevMode, sizeof(DEVMODE));
		DEVMODE* dm = (DEVMODE*)HdevMode;
		dm->dmSize = sizeof(DEVMODE);
		//dm->dmCopies = 3;
	}
	else if (HdevMode != NULL)
	{
		// Set to landscape orientation
		DEVMODE* dm = (DEVMODE*)HdevMode;
		dm->dmFields = DM_ORIENTATION;
		dm->dmOrientation = DMORIENT_LANDSCAPE;
	}

	// Initialize PRINTDLG
	PRINTDLG pd = {0};
	ZeroMemory(&pd, sizeof(PRINTDLG));
	pd.lStructSize = sizeof(PRINTDLG);
	pd.hwndOwner   = hWndPOSAssist; //hwnd;
	pd.hDevMode    = HdevMode;      // Don't forget to free or store hDevMode.
	pd.Flags       = PD_RETURNDC | PD_NOPAGENUMS | PD_USEDEVMODECOPIESANDCOLLATE | PD_NOSELECTION;
	pd.nCopies     = 1; //3; // [TBC] -- Config Parameter, Refer SMITDev #156
	pd.nFromPage	= 0xFFFF;
	pd.nToPage		= 0xFFFF;
	pd.nMinPage		= 1;
	pd.nMaxPage		= 0xFFFF;

	cxpage = GetDeviceCaps (prn, HORZRES);
	cypage = GetDeviceCaps (prn, VERTRES);
	hdcMem = CreateCompatibleDC(prn);

	StartDoc (prn, &di);
	StartPage (prn);
	SetMapMode (prn, MM_ISOTROPIC);
	SetWindowExtEx(prn, cxpage,cypage, NULL);
	SetViewportExtEx(prn, cxpage, cypage,NULL);

	SetViewportOrgEx(prn, 0, 0, NULL);
	StretchBlt(prn, 0, 0, cxpage, cypage, hdcMem, 0, 0, 412, 313, SRCCOPY);

// == End: Printing Prepare Session =======================================================
    int org_x = 3;
	int org_y = 3;
	int line_step = 80;

	hFont = CreateFont(100, 12, 0, 0, FW_NORMAL,FALSE,FALSE,FALSE, THAI_CHARSET,OUT_CHARACTER_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("AngsanaUPC"));
	hFont20 = CreateFont(85, 10, 0, 0, FW_NORMAL,FALSE,FALSE,FALSE, THAI_CHARSET,OUT_CHARACTER_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY, FIXED_PITCH  ,TEXT("AngsanaUPC"));

	SelectObject(prn, hFont);
	TextOut(prn, org_x, org_y, prnText[0], lstrlen(prnText[0]));

	int _i_line = 1;
	SelectObject(prn, hFont20);
	for (int i=_i_line; i < _prn_lines; i++) {
		TextOut(prn, org_x, org_y + (line_step * _i_line), prnText[i], lstrlen(prnText[i]));
		_i_line++;
		
		if ((i == 2) || (i == (_prn_lines-2)) || (i == (_prn_lines-1))) {
			TextOut(prn, org_x, org_y + (line_step * _i_line), line_string, lstrlen(line_string));
			_i_line++;
		}
	}

	DeleteObject(hFont);
	DeleteObject(hFont20);
// == Start to print below ===============================================================

	EndPage(prn);
	EndDoc(prn);
	DeleteDC(prn);
	DeleteDC(hdcMem);

	return 0;
}


INT Operate_RFID_Read()
{
	char chr_buff[512];
	TCHAR tch_returnID[512]; /** HEX format data in string */

	int ret = 0;
	ret = sl500_req_card_ID(chr_buff, 512);

	if (ret >= 0) {
		/**
		Using ATL 7.0 String Conversion Classes and Macros
		Credit: http://stackoverflow.com/questions/16342976/how-to-convert-char-to-tchar
		Ref: https://msdn.microsoft.com/en-GB/library/87zae4a3(v=vs.110).aspx#atl70stringconversionclassesmacros

		Do convert "ANSI character string" to "Generic character string
		(equivalent to W when _UNICODE is defined, equivalent to A otherwise)."
		**/
		_tcscpy(tch_returnID, CA2T(chr_buff));

		/** Return the Card ID to the connected Application */
		sprintf_s(g_szReturnSocketRAW, "%c%S%s%S%s%S",
			CMD_HEADER,
			CMD_RFID_REQ_CARD_ID_SL500,
			SPLIT_TEXT,
			STATUS_OK,
			SPLIT_TEXT,
			tch_returnID);
	}
	else {
		/** Return the fault status */
		sprintf_s(g_szReturnSocketRAW, "%c%S%s%S",
			CMD_HEADER,
			CMD_RFID_REQ_CARD_ID_SL500,
			SPLIT_TEXT,
			STATUS_NG);
	}
	nReturnSocketRAW = strlen(g_szReturnSocketRAW);

	/** Signal to the user */
	sl500_beep_ready();
	return 0;
}

