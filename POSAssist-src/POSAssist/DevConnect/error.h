#pragma once

#ifndef MYERROR_H
#define MYERROR_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#define LOG_LEVEL_LENGTH	128
#define LOG_LEVEL_SILENT	TEXT("silent")
#define LOG_LEVEL_DEBUG		TEXT("debug")
#define LOG_LEVEL_INFO		TEXT("info")
#define LOG_LEVEL_NOTICE	TEXT("notice")
#define LOG_LEVEL_WARN		TEXT("warn")
//#define LOG_LEVEL_DEBUG ... ..
#define LOG_LEVEL_DEFAULT	LOG_LEVEL_INFO

void error(TCHAR *fmt, ...);
void errend(TCHAR *fmt, ...);
void perror2(const char *fmt, ...);
void perrend(TCHAR *fmt, ...);
void to_log_file(TCHAR *log_buff);
void dbg_msg(TCHAR *fmt, ...);

extern TCHAR szlog_level[LOG_LEVEL_LENGTH];

#if (defined (_DEBUG) || defined (DEBUG))
#define DBG_MSG1(x) dbg_msg((x))
#define DBG_MSG2(x,y) dbg_msg((x),(y))
#define DBG_MSG3(x,y,z) dbg_msg((x),(y),(z))
#else
#define DBG_MSG1(x) ;
#define DBG_MSG2(x,y) ;
#define DBG_MSG3(x,y,z) ;
#endif


#endif	/* MYERROR_H */
