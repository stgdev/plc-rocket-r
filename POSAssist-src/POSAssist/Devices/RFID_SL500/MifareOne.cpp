//#include "stdafx.h"
// What's the use for �stdafx.h� in Visual Studio?
//
// Goto SolutionExplorer (should be already visible, if not use menu: View->SolutionExplorer).
//
// Find your.cxx file in the solution tree, right click on it and choose "Properties" 
// from the popup menu.You will get window with your file's properties.
//
// Using tree on the left side go to the "C++/Precompiled Headers" section.
// On the right side of the window you'll get three properties. Set property named 
// "Create/Use Precompiled Header" to the value of "Not Using Precompiled Headers".
//
// http://stackoverflow.com/questions/8862840/unexpected-end-of-file-error
// http://stackoverflow.com/questions/4726155/whats-the-use-for-stdafx-h-in-visual-studio/4726838#4726838
//


#if defined(__WIN32__)
#include <windows.h>
#elif defined(__UNIX__)
#endif

#include "MifareOne.h"

#if defined(MIFAREONE_WIN32)
#include "ExportFunc.h"
#endif

#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <wchar.h>

#include "../../DevConnect/error.h"

#define SHORT_BEEP		3
#define MIDDLE_BEEP		6

HINSTANCE m_hInstMaster;
unsigned short m_devID;

int sl500_init()
{

#if defined(__WIN32__)
	WCHAR szBuff[MAX_PATH] = _T("\\MasterRD.dll");
	GetModuleFileName(NULL, (LPTSTR)szBuff, MAX_PATH);
	*wcsrchr(szBuff, '\\') = 0; /* use wcsrchr() instead of strrchr() */
	wcscat_s(szBuff, _T("\\MasterRD.dll")); /* use wcscat_s() instead of strcat() */

	m_hInstMaster = LoadLibrary((LPTSTR)szBuff);

	if (m_hInstMaster) {
		(FARPROC&)lib_ver = GetProcAddress(m_hInstMaster, "lib_ver");
		(FARPROC&)des_encrypt = GetProcAddress(m_hInstMaster, "des_encrypt");
		(FARPROC&)des_decrypt = GetProcAddress(m_hInstMaster, "des_decrypt");
		(FARPROC&)rf_init_com = GetProcAddress(m_hInstMaster, "rf_init_com");
		(FARPROC&)rf_init_device_number = GetProcAddress(m_hInstMaster, "rf_init_device_number");
		(FARPROC&)rf_get_device_number = GetProcAddress(m_hInstMaster, "rf_get_device_number");
		(FARPROC&)rf_get_model = GetProcAddress(m_hInstMaster, "rf_get_model");
		(FARPROC&)rf_get_snr = GetProcAddress(m_hInstMaster, "rf_get_snr");
		(FARPROC&)rf_beep = GetProcAddress(m_hInstMaster, "rf_beep");
		(FARPROC&)rf_init_sam = GetProcAddress(m_hInstMaster, "rf_init_sam");
		(FARPROC&)rf_sam_rst = GetProcAddress(m_hInstMaster, "rf_sam_rst");
		(FARPROC&)rf_sam_cos = GetProcAddress(m_hInstMaster, "rf_sam_cos");
		(FARPROC&)rf_init_type = GetProcAddress(m_hInstMaster, "rf_init_type");
		(FARPROC&)rf_antenna_sta = GetProcAddress(m_hInstMaster, "rf_antenna_sta");
		(FARPROC&)rf_request = GetProcAddress(m_hInstMaster, "rf_request");
		(FARPROC&)rf_anticoll = GetProcAddress(m_hInstMaster, "rf_anticoll");
		(FARPROC&)rf_select = GetProcAddress(m_hInstMaster, "rf_select");
		(FARPROC&)rf_halt = GetProcAddress(m_hInstMaster, "rf_halt");
		(FARPROC&)rf_download_key = GetProcAddress(m_hInstMaster, "rf_download_key");
		(FARPROC&)rf_M1_authentication1 = GetProcAddress(m_hInstMaster, "rf_M1_authentication1");
		(FARPROC&)rf_M1_authentication2 = GetProcAddress(m_hInstMaster, "rf_M1_authentication2");
		(FARPROC&)rf_M1_read = GetProcAddress(m_hInstMaster, "rf_M1_read");
		(FARPROC&)rf_M1_write = GetProcAddress(m_hInstMaster, "rf_M1_write");
		(FARPROC&)rf_M1_initval = GetProcAddress(m_hInstMaster, "rf_M1_initval");
		(FARPROC&)rf_M1_readval = GetProcAddress(m_hInstMaster, "rf_M1_readval");
		(FARPROC&)rf_M1_decrement = GetProcAddress(m_hInstMaster, "rf_M1_decrement");
		(FARPROC&)rf_M1_increment = GetProcAddress(m_hInstMaster, "rf_M1_increment");
		(FARPROC&)rf_M1_restore = GetProcAddress(m_hInstMaster, "rf_M1_restore");
		(FARPROC&)rf_M1_transfer = GetProcAddress(m_hInstMaster, "rf_M1_transfer");
		(FARPROC&)rf_typea_rst = GetProcAddress(m_hInstMaster, "rf_typea_rst");
		(FARPROC&)rf_cos_command = GetProcAddress(m_hInstMaster, "rf_cos_command");
		(FARPROC&)rf_atqb = GetProcAddress(m_hInstMaster, "rf_atqb");
		(FARPROC&)rf_attrib = GetProcAddress(m_hInstMaster, "rf_attrib");
		(FARPROC&)rf_typeb_cos = GetProcAddress(m_hInstMaster, "rf_typeb_cos");
		(FARPROC&)rf_hltb = GetProcAddress(m_hInstMaster, "rf_hltb");
		(FARPROC&)rf_at020_check = GetProcAddress(m_hInstMaster, "rf_at020_check");
		(FARPROC&)rf_at020_read = GetProcAddress(m_hInstMaster, "rf_at020_read");
		(FARPROC&)rf_at020_write = GetProcAddress(m_hInstMaster, "rf_at020_write");
		(FARPROC&)rf_at020_lock = GetProcAddress(m_hInstMaster, "rf_at020_lock");
		(FARPROC&)rf_at020_count = GetProcAddress(m_hInstMaster, "rf_at020_count");
		(FARPROC&)rf_at020_deselect = GetProcAddress(m_hInstMaster, "rf_at020_deselect");
		(FARPROC&)rf_light = GetProcAddress(m_hInstMaster, "rf_light");
		(FARPROC&)rf_ClosePort = GetProcAddress(m_hInstMaster, "rf_ClosePort");
		(FARPROC&)rf_GetErrorMessage = GetProcAddress(m_hInstMaster, "rf_GetErrorMessage");

		if (NULL == lib_ver ||
			NULL == des_encrypt ||
			NULL == des_decrypt ||
			NULL == rf_init_com ||
			NULL == rf_init_device_number ||
			NULL == rf_get_device_number ||
			NULL == rf_get_model ||
			NULL == rf_beep ||
			NULL == rf_init_sam ||
			NULL == rf_sam_rst ||
			NULL == rf_sam_cos ||
			NULL == rf_init_type ||
			NULL == rf_antenna_sta ||
			NULL == rf_request ||
			NULL == rf_anticoll ||
			NULL == rf_select ||
			NULL == rf_halt ||
			NULL == rf_download_key ||
			NULL == rf_M1_authentication1 ||
			NULL == rf_M1_authentication2 ||
			NULL == rf_M1_read ||
			NULL == rf_M1_write ||
			NULL == rf_M1_initval ||
			NULL == rf_M1_readval ||
			NULL == rf_M1_decrement ||
			NULL == rf_M1_increment ||
			NULL == rf_M1_restore ||
			NULL == rf_M1_transfer ||
			NULL == rf_typea_rst ||
			NULL == rf_cos_command ||
			NULL == rf_atqb ||
			NULL == rf_attrib ||
			NULL == rf_typeb_cos ||
			NULL == rf_hltb ||
			NULL == rf_at020_check ||
			NULL == rf_at020_read ||
			NULL == rf_at020_write ||
			NULL == rf_at020_lock ||
			NULL == rf_at020_count ||
			NULL == rf_at020_deselect ||
			NULL == rf_light ||
			NULL == rf_ClosePort ||
			NULL == rf_GetErrorMessage)
		{
			MessageBox(NULL, (LPCWSTR)L"Load MasterRD.dll failed !", (LPCWSTR)L"Error", MB_OK | MB_ICONERROR);
		}
	}

	int state = 1;
	state = rf_init_com(9, 115200);

	if (state != LIB_SUCCESS) {
		rf_ClosePort();
		MessageBox(NULL, (LPCWSTR)L"Open port error!", (LPCWSTR)L"Error", MB_OK | MB_ICONERROR);
		return -1;
	}

	state = rf_get_device_number(&m_devID);
	DBG_MSG2(TEXT("Returned SL500 Device ID 0x%X \n"), m_devID);

	state = rf_beep(m_devID, SHORT_BEEP);

#endif

	return 0;
}

int sl500_close()
{
	int state = 1;

#if defined(__WIN32__)
	state = rf_beep(m_devID, MIDDLE_BEEP);

	rf_ClosePort();

	//Release masterRD.dll
	if (m_hInstMaster) FreeLibrary(m_hInstMaster);
#endif

	return 0;
}

int sl500_req_card_ID(char *pBuff, int buff_length)
{
	/*
	* Original code is from etc/RFID_SL500_EXAMPLE/VC/MifareOneDemoDlg.cpp
	* CMifareOneDemoDlg::OnButtonSearch()
	*/

	WORD icdev = 0x0000;
	unsigned char mode = 0x52;
	int status;
	unsigned short TagType;
	unsigned char bcnt = 0x04;//mifare card use 0x04
	unsigned char Snr[MAX_RF_BUFFER];
	unsigned char len;
	unsigned char Size;

#if defined(__WIN32__)
	status = rf_request(icdev, mode, &TagType);//search all card
	if (status) {//error
		return -1;
	}

	status = rf_anticoll(icdev, bcnt, Snr, &len);//return serial number of card
	if (status || len != 4) { //error
		return -2;
	}

	status = rf_select(icdev, Snr, len, &Size);//lock ISO14443-3 TYPE_A 
	if (status) {//error
		return -3;
	}
#endif

	// Sample Snr: 0x40 0x17 0x78 0x5D
	// Have to convert the Big Endian to Little Endian then return
	// 0x5D781740 == 1568151360
	sprintf(pBuff, "0x%02X%02X%02X%02X", Snr[3], Snr[2], Snr[1], Snr[0]);
	DBG_MSG3(TEXT("Return ID %S - with %d bytes from SL500\n"), pBuff, len);

	return 0;
}

int sl500_beep_ready()
{
	int status;
	status = rf_beep(m_devID, SHORT_BEEP);
	return 0;
}
