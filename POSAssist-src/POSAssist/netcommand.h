#ifndef NETCOMMAND_H
#define NETCOMMAND_H

/**
* Data Stucture
*  -> Sending Command
*  [HEADER][Command]
*
*  <- Return Message
*  [STATUS][Message or Return value]
*/

#define CMD_HEADER				'\t'
#define CMD_UTF_HEADER			'\v'
#define SPLIT_TEXT				TEXT("\\")
#define MIN_CMD_LENGTH			5
#define UNLOCK_BYTE				0xAA

/** [STATUS] */
#define STATUS_OK				TEXT("OK")
#define STATUS_NG				TEXT("NG")

/** [Message] */
#define UNKNOWN_MSG				TEXT("UNKNOWN_COMMAND")
#define FIFO_EMPTY				TEXT("FIFO_EMPTY")
#define FIFO_BUSY				TEXT("FIFO_BUSY")
#define CADR_OPEN				TEXT("CADR_OPEN")
#define CADR_CLOSE				TEXT("CADR_CLOSE")

/** [Command] */
#define CMD_GET_BSCLINE			TEXT("GET BSCLINE")
#define CMD_GET_CADRSTAT		TEXT("GET CADRSTAT")
#define CMD_UNLOCK_CADR			TEXT("UNLOCKCADR")
#define CMD_FORCE_PRINT			TEXT("FORCEPRINT")
#define CMD_PRINT_LABEL			TEXT("PRINT_LABEL")
#define CMD_KITCHEN_PRINT		TEXT("KITCHEN_PRINT")
#define CMD_RFID_INIT_SL500		TEXT("RFID_INIT_SL500")
#define CMD_RFID_CLOSE_SL500	TEXT("RFID_CLOSE_SL500")
#define CMD_RFID_REQ_CARD_ID_SL500		TEXT("RFID_REQ_CARD_ID_SL500")

#endif /* NETCOMMAND */
