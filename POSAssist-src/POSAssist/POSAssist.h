#pragma once

#include "resource.h"

#define MAX_LOADSTRING			100
#define WM_POSASSIST_SOCKET		WM_USER+1
#define WM_CHK_SOCKINPUT		WM_USER+2
#define WM_POSASSIST_SYSTRAY	WM_USER+3

#define POSASSIST_EXT_PORT		5333
#define SOCKETRAW_BUFFER_SIZE	1024 //512
#define HALF_BUFSIZ				BUFSIZ/2
#define PRINTER_NAME_SIZE		HALF_BUFSIZ

// Window system variables:
HINSTANCE	hInst;								// current instance
HWND		hWndPOSAssist;						// current window handler
TCHAR		szTitle[MAX_LOADSTRING];			// The title bar text
TCHAR		szWindowClass[MAX_LOADSTRING];		// the main window class name
NOTIFYICONDATA	iconSystemTray;
BOOL		bHideWnd;

// Barcode Scanner releated variables:
HANDLE		hBarcodeScanner = NULL;				// To handle Barcode Scanner Device
TCHAR		szScanData[MAX_LOADSTRING];			// Collect and store input from Barcode Scanner
TCHAR		szStoreCh[MAX_LOADSTRING];
TCHAR		szBSCFIFO[MAX_LOADSTRING];

// Socket and network interface variables:
char		g_szSocketRAW[SOCKETRAW_BUFFER_SIZE];
INT			nChSockRAW;
INT			isCMDSockRAW;
char		g_szReturnSocketRAW[SOCKETRAW_BUFFER_SIZE];
INT			nReturnSocketRAW;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT	CALLBACK	SystemTrayAct(LPARAM lParam);
VOID				HideWindow();

// Additional functions: ///////////////////////////////////
INT					SearchDevice();
INT					FilterSocketRAW(SOCKET callSocket);
INT					OperateNetCMD(WCHAR *pCMDBuff);
INT					DirectPrintLabel(WCHAR *pTextCmdBuff);
INT					DirectPrintOrderList(WCHAR *pTextCmdBuff);
pipe_s*				FindPipe(int port);
INT					ConvNativeStringToTChar(char *pchSrc, size_t szSrc, TCHAR *pTarget);
INT					Operate_RFID_Read();

// DevConnect service routines & variables /////////////////
int					readcfg(void);
void				cleanup(void);
void				debug(void);
thr_startfunc_t		serve_pipe(void *data);
thr_startfunc_t		serve_pipe_ext(void *data);
int					preWaitClients();
int					findInvolvedSocket(SOCKET reqSocket);

#if defined(__UNIX__)
char cfgfile[] = "/etc/devcn.cfg";
#elif defined(__WIN32__)
char cfgfile[] = "devcn.cfg";
#endif

cfg_s		cfg;
vlist_s		pipes;
TCHAR		tchPrinterName[PRINTER_NAME_SIZE];
TCHAR		tchNetPrinterName[PRINTER_NAME_SIZE];
////////////////////////////////////////////////////////////